package br.com.digio.services;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import br.com.digio.domains.ContaContabil;
import br.com.digio.domains.Lancamento;
import br.com.digio.exceptions.ContaContabilNotFoundException;
import br.com.digio.models.StatsModel;
import br.com.digio.repositories.ContaContabilRepository;
import br.com.digio.repositories.LancamentoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class LancamentoServiceTest {
    @InjectMocks
    private LancamentoService lancamentoService;

    @Mock
    private LancamentoRepository lancamentoRepository;

    @Mock
    private ContaContabilRepository contaContabilRepository;

    private static final UUID UUID_FOR_TESTS = UUID.fromString("d869828c-10f4-4044-b614-14a310ebf961");

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getById() {
        Optional<Lancamento> lancamentoDb = createLancamento1500();
        when(lancamentoRepository.findById(UUID_FOR_TESTS)).thenReturn(lancamentoDb);
        Lancamento lancamento = lancamentoService.getById(UUID_FOR_TESTS);
        Assert.notNull(lancamento, "Lancamento should not be null");
    }

    @Test
    public void getByIdLancamentoNull() {
        Optional<Lancamento> lancamentoDb = Optional.empty();
        when(lancamentoRepository.findById(UUID_FOR_TESTS)).thenReturn(lancamentoDb);
        Lancamento lancamento = lancamentoService.getById(UUID_FOR_TESTS);
        Assert.isNull(lancamento, "Lancamento should be null");
    }

    @Test(expected = ContaContabilNotFoundException.class)
    public void getByContaContabilContaNotFound() throws Exception {
        Optional<ContaContabil> contaContabilDb = Optional.empty();
        when(contaContabilRepository.findById(anyLong())).thenReturn(contaContabilDb);
        lancamentoService.getByContaContabil(new ContaContabil());
    }

    @Test
    public void getByContaContabil() throws Exception {
        Optional<ContaContabil> contaContabilDb = Optional.of(new ContaContabil());
        List<Lancamento> lancamentosDb = new ArrayList<>();
        lancamentosDb.add(createLancamento1500().get());
        when(contaContabilRepository.findById(anyLong())).thenReturn(contaContabilDb);
        when(lancamentoRepository.findByContaContabil(contaContabilDb.get())).thenReturn(lancamentosDb);
        List<Lancamento> lancamentos = lancamentoService.getByContaContabil(contaContabilDb.get());
        Assert.notEmpty(lancamentos, "Lancamentos should not be empty");
    }

    @Test
    public void getStatsAll() throws Exception {
        Optional<ContaContabil> contaContabilDb = Optional.of(new ContaContabil());
        when(lancamentoRepository.count()).thenReturn(2L);
        when(lancamentoRepository.findMax()).thenReturn(new BigDecimal("2500.0"));
        when(lancamentoRepository.findSum()).thenReturn(new BigDecimal("4000.0"));
        when(lancamentoRepository.findMin()).thenReturn(new BigDecimal("1500.0"));
        StatsModel stats = lancamentoService.getStats(contaContabilDb.get());
        Assert.notNull(stats, "StatsModel should not be empty");
        Assert.isTrue(stats.getMax().compareTo(new BigDecimal("2500.0")) == 0, "Max should be equal");
        Assert.isTrue(stats.getMin().compareTo(new BigDecimal("1500.0")) == 0, "Min should be equal");
        Assert.isTrue(stats.getSoma().compareTo(new BigDecimal("4000.0")) == 0, "Soma should be equal");
        Assert.isTrue(stats.getQtde() == 2, "Qtde should be equal");
    }

    @Test
    public void getStatsAllNull() throws Exception {
        Optional<ContaContabil> contaContabilDb = Optional.of(new ContaContabil());
        when(lancamentoRepository.count()).thenReturn(0L);
        StatsModel stats = lancamentoService.getStats(contaContabilDb.get());
        Assert.isNull(stats, "StatsModel should be null");
    }

    @Test
    public void getStatsByContaContabil() throws Exception {
        ContaContabil contaContabil = new ContaContabil();
        contaContabil.setId(111);
        Optional<ContaContabil> contaContabilDb = Optional.of(contaContabil);
        List<Lancamento> lancamentosDb = new ArrayList<>();
        lancamentosDb.add(createLancamento1500().get());
        lancamentosDb.add(createLancamento2500().get());
        when(contaContabilRepository.findById(111L)).thenReturn(contaContabilDb);
        when(lancamentoRepository.findByContaContabil(contaContabilDb.get())).thenReturn(lancamentosDb);
        StatsModel stats = lancamentoService.getStats(contaContabilDb.get());
        Assert.notNull(stats, "StatsModel should not be empty");
        Assert.isTrue(stats.getMax().compareTo(new BigDecimal("2500.0")) == 0, "Max should be equal");
        Assert.isTrue(stats.getMin().compareTo(new BigDecimal("1500.0")) == 0, "Min should be equal");
        Assert.isTrue(stats.getSoma().compareTo(new BigDecimal("4000.0")) == 0, "Soma should be equal");
        Assert.isTrue(stats.getQtde() == 2, "Qtde should be equal");
    }

    @Test
    public void getStatsByContaContabilNull() throws Exception {
        ContaContabil contaContabil = new ContaContabil();
        contaContabil.setId(111);
        Optional<ContaContabil> contaContabilDb = Optional.of(contaContabil);
        when(contaContabilRepository.findById(111L)).thenReturn(contaContabilDb);
        when(lancamentoRepository.findByContaContabil(contaContabilDb.get())).thenReturn(null);
        StatsModel stats = lancamentoService.getStats(contaContabilDb.get());
        Assert.isNull(stats, "StatsModel should be empty");
    }
    @Test(expected = ContaContabilNotFoundException.class)
    public void getStatsByContaContabilNotFoundConta() throws Exception {
        Optional<ContaContabil> contaContabilDb = Optional.empty();
        when(contaContabilRepository.findById(anyLong())).thenReturn(contaContabilDb);
        ContaContabil contaContabil = new ContaContabil();
        contaContabil.setId(222);
        lancamentoService.getStats(contaContabil);
    }

    private Optional<Lancamento> createLancamento1500() {
        Lancamento lancamento = new Lancamento();
        lancamento.setId(UUID_FOR_TESTS);
        lancamento.setValor(new BigDecimal("1500.0"));
        lancamento.setData(LocalDate.now());
        ContaContabil contaContabil = new ContaContabil();
        contaContabil.setId(10001);
        contaContabil.setDescricao("descricao da conta");
        lancamento.setContaContabil(contaContabil);
        return Optional.of(lancamento);
    }

    private Optional<Lancamento> createLancamento2500() {
        Lancamento lancamento = new Lancamento();
        lancamento.setId(UUID_FOR_TESTS);
        lancamento.setValor(new BigDecimal("2500.0"));
        lancamento.setData(LocalDate.now());
        ContaContabil contaContabil = new ContaContabil();
        contaContabil.setId(10001);
        contaContabil.setDescricao("descricao da conta");
        lancamento.setContaContabil(contaContabil);
        return Optional.of(lancamento);
    }
}
