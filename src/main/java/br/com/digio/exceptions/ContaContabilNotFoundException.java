package br.com.digio.exceptions;

public class ContaContabilNotFoundException extends Exception {
    public ContaContabilNotFoundException() {
    }

    public ContaContabilNotFoundException(Throwable t) {
        super(t);
    }

    public ContaContabilNotFoundException(String message) {
        super(message);
    }

    public ContaContabilNotFoundException(String message, Throwable t) {
        super(message, t);
    }
}
