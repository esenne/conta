package br.com.digio.exceptions;

public class CriticalException extends Exception {
    public CriticalException() {
    }

    public CriticalException(Throwable t) {
        super(t);
    }

    public CriticalException(String message) {
        super(message);
    }

    public CriticalException(String message, Throwable t) {
        super(message, t);
    }
}
