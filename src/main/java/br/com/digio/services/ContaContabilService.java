package br.com.digio.services;

import br.com.digio.domains.ContaContabil;
import br.com.digio.repositories.ContaContabilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContaContabilService {
    @Autowired
    private ContaContabilRepository contaContabilRepository;

    public ContaContabil save(ContaContabil contaContabil) {
        return contaContabilRepository.save(contaContabil);
    }
}
