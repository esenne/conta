package br.com.digio.services;

import br.com.digio.domains.ContaContabil;
import br.com.digio.domains.Lancamento;
import br.com.digio.exceptions.ContaContabilNotFoundException;
import br.com.digio.models.StatsModel;
import br.com.digio.repositories.ContaContabilRepository;
import br.com.digio.repositories.LancamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LancamentoService {
    @Autowired
    private LancamentoRepository lancamentoRepository;
    @Autowired
    private ContaContabilRepository contaContabilRepository;

    public Lancamento save(Lancamento lancamento) throws ContaContabilNotFoundException {
        Optional<ContaContabil> contaContabil = contaContabilRepository.findById(lancamento.getContaContabil().getId());
        if (!contaContabil.isPresent()) {
            throw new ContaContabilNotFoundException("ContaContabil não encontrada: " + lancamento.getContaContabil().getId());
        }
        return lancamentoRepository.save(lancamento);
    }

    public Lancamento getById(UUID uuid) {
        Optional<Lancamento> lancamentoOptional = lancamentoRepository.findById(uuid);
        if (lancamentoOptional.isPresent()) {
            return lancamentoOptional.get();
        }
        return null;
    }

    public List<Lancamento> getByContaContabil(ContaContabil contaContabil) throws ContaContabilNotFoundException {
        Optional<ContaContabil> contaContabilOptional = contaContabilRepository.findById(contaContabil.getId());
        if (!contaContabilOptional.isPresent()) {
            throw new ContaContabilNotFoundException("ContaContabil não encontrada: " + contaContabil.getId());
        }
        return lancamentoRepository.findByContaContabil(contaContabil);
    }

    public StatsModel getStats(ContaContabil contaContabil) throws ContaContabilNotFoundException {
        StatsModel statsModel = null;
        if (contaContabil.getId() == 0) {
            statsModel = getStatsAll();
        } else {
            Optional<ContaContabil> contaContabilOptional = contaContabilRepository.findById(contaContabil.getId());
            if (!contaContabilOptional.isPresent()) {
                throw new ContaContabilNotFoundException("ContaContabil não encontrada: " + contaContabil.getId());
            }
            statsModel = getStatsByContaContabil(contaContabil);
        }
        return statsModel;
    }

    private StatsModel getStatsByContaContabil(ContaContabil contaContabil) {
        StatsModel statsModel = new StatsModel();
        List<Lancamento> lancamentos = lancamentoRepository.findByContaContabil(contaContabil);
        if (lancamentos == null) {
            return null;
        }
        Optional<BigDecimal> max = lancamentos.stream()
                .map(a -> a.getValor())
                .max(Comparator.naturalOrder());
        if (max.isPresent()) {
            statsModel.setMax(max.get());
        }
        Optional<BigDecimal> min = lancamentos.stream()
                .map(a -> a.getValor())
                .min(Comparator.naturalOrder());
        if (min.isPresent()) {
            statsModel.setMin(min.get());
        }
        BigDecimal sum = lancamentos.stream().map(Lancamento::getValor)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        statsModel.setSoma(sum);
        statsModel.setQtde(lancamentos.size());
        return statsModel;
    }

    private StatsModel getStatsAll() {
        long qtde = lancamentoRepository.count();
        if (qtde == 0) {
            return null;
        }
        StatsModel statsModel = new StatsModel();
        statsModel.setMax(lancamentoRepository.findMax());
        statsModel.setMin(lancamentoRepository.findMin());
        statsModel.setQtde(qtde);
        statsModel.setSoma(lancamentoRepository.findSum());
        statsModel.setMedia(lancamentoRepository.findAverage());
        return statsModel;
    }
}
