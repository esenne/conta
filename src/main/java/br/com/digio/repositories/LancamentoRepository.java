package br.com.digio.repositories;

import br.com.digio.domains.ContaContabil;
import br.com.digio.domains.Lancamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface LancamentoRepository extends JpaRepository<Lancamento, UUID> {
    List<Lancamento> findByContaContabil(ContaContabil contaContabil);

    @Query("SELECT max(l.valor) FROM Lancamento l")
    BigDecimal findMax();

    @Query("SELECT min(l.valor) FROM Lancamento l")
    BigDecimal findMin();

    @Query("SELECT sum(l.valor) FROM Lancamento l")
    BigDecimal findSum();

    @Query("SELECT avg(l.valor) FROM Lancamento l")
    BigDecimal findAverage();
}
