package br.com.digio.repositories;

import br.com.digio.domains.ContaContabil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContaContabilRepository  extends JpaRepository<ContaContabil, Long> {
}
