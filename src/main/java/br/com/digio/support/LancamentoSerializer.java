package br.com.digio.support;

import br.com.digio.domains.Lancamento;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class LancamentoSerializer extends StdSerializer<Lancamento> {
    public LancamentoSerializer() {
        this(null);
    }

    public LancamentoSerializer(Class<Lancamento> t) {
        super(t);
    }

    @Override
    public void serialize(Lancamento value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeNumberField("contaContabil", value.getContaContabil().getId());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        jgen.writeNumberField("data", Long.valueOf(value.getData().format(formatter)));
        jgen.writeNumberField("valor", value.getValor());
        jgen.writeEndObject();
    }
}
