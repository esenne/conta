package br.com.digio.support;

import br.com.digio.domains.ContaContabil;
import br.com.digio.domains.Lancamento;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LancamentoDeserializer extends StdDeserializer<Lancamento> {
    public LancamentoDeserializer() {
        this(null);
    }

    public LancamentoDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Lancamento deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        long conta = 0;
        JsonNode contaNode = node.get("contaContabil");
        if (contaNode != null) {
            conta = contaNode.asLong();
        }
        int data = 0;
        JsonNode dataNode = node.get("data");
        if (dataNode != null) {
            data = dataNode.asInt();
        }
        BigDecimal valor = null;
        JsonNode valorNode = node.get("valor");
        if (valorNode != null) {
            valor = new BigDecimal(node.get("valor").asText());
        }
        Lancamento lancamento = new Lancamento();
        if (data != 0) {
            lancamento.setData(LocalDate.parse(String.valueOf(data), DateTimeFormatter.ofPattern("yyyyMMdd")));
        }
        if (valor != null) {
            lancamento.setValor(valor);
        }
        if (conta != 0) {
            ContaContabil contaContabil = new ContaContabil();
            contaContabil.setId(conta);
            lancamento.setContaContabil(contaContabil);
        }
        return lancamento;
    }
}
