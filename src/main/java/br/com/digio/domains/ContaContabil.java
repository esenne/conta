package br.com.digio.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CONTA_CONTABIL")
public class ContaContabil {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;
    @Column(name = "DESCRICAO")
    private String descricao;
    @OneToMany(mappedBy = "contaContabil")
    @JsonIgnore
    private List<Lancamento> lancamentos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(List<Lancamento> lancamentos) {
        this.lancamentos = lancamentos;
    }
}
