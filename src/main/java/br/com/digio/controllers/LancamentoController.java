package br.com.digio.controllers;

import br.com.digio.domains.ContaContabil;
import br.com.digio.domains.Lancamento;
import br.com.digio.exceptions.ContaContabilNotFoundException;
import br.com.digio.models.StatsModel;
import br.com.digio.services.LancamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
public class LancamentoController {
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    @Autowired
    private LancamentoService lancamentoService;

    @PostMapping(value = "/lancamentos-contabeis/")
    public ResponseEntity<Map<String, String>> saveLancamento(@RequestBody Lancamento lancamento) {
        Map<String, String> mapWithErrors =  validateLancamento(lancamento);
        if (mapWithErrors != null) {
            return new ResponseEntity<Map<String, String>>(mapWithErrors, HttpStatus.BAD_REQUEST);
        }
        try {
            lancamento = lancamentoService.save(lancamento);
        } catch (ContaContabilNotFoundException e) {
            return new ResponseEntity<Map<String, String>>(createErrorMessageMap(e), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<Map<String, String>>(createErrorMessageMap("Ocorreu um erro inesperado"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Map<String, String> mapWithId = new HashMap<>();
        mapWithId.put("id", lancamento.getId().toString());
        return new ResponseEntity<Map<String, String>>(mapWithId, HttpStatus.CREATED);
    }

    @GetMapping("/lancamentos-contabeis/{id}")
    public ResponseEntity<Lancamento> getLancamento(@PathVariable String id) {
        Lancamento lancamento = lancamentoService.getById(UUID.fromString(id));
        if (lancamento == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Lancamento>(lancamento, HttpStatus.OK);
    }

    @GetMapping("/lancamentos-contabeis/")
    public ResponseEntity<List<Lancamento>> getLancamentoByContaContabil(@RequestParam(value="contaContabil") long contaContabil) {
        ContaContabil conta = new ContaContabil();
        conta.setId(contaContabil);
        List<Lancamento> lancamentos = null;
        try {
            lancamentos = lancamentoService.getByContaContabil(conta);
        } catch (ContaContabilNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (lancamentos == null || lancamentos.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Lancamento>>(lancamentos, HttpStatus.OK);
    }

    @GetMapping("/lancamentos-contabeis/stats/")
    public ResponseEntity<StatsModel> getLancamentoStats(@RequestParam(value="contaContabil", required = false) Long contaContabil) {
        ContaContabil conta = new ContaContabil();
        if (contaContabil != null) {
            conta.setId(contaContabil);
        }
        StatsModel statsModel = null;
        try {
            statsModel = lancamentoService.getStats(conta);
        } catch (ContaContabilNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (statsModel == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<StatsModel>(statsModel, HttpStatus.OK);
    }

    private Map<String, String> createErrorMessageMap(Exception e) {
        Map<String, String> mapWithMessage = new HashMap<>();
        mapWithMessage.put("erro", e.getMessage());
        return mapWithMessage;
    }

    private Map<String, String> createErrorMessageMap(String mensagem) {
        Map<String, String> mapWithMessage = new HashMap<>();
        mapWithMessage.put("erro", mensagem);
        return mapWithMessage;
    }

    private Map<String, String> validateLancamento(Lancamento lancamento) {
        Map<String, String> mapWithMessage = new HashMap<>();
        StringBuffer message = new StringBuffer();
        if (lancamento.getValor() == null) {
            message.append("Valor inválido.");
            message.append(LINE_SEPARATOR);
        }
        if (lancamento.getContaContabil() == null) {
            message.append("ContaContabil inválida.");
            message.append(LINE_SEPARATOR);
        }
        if (lancamento.getData() == null) {
            message.append("Data inválida.");
            message.append(LINE_SEPARATOR);
        }
        if (message.length() > 0) {
            mapWithMessage.put("erro", message.toString());
            return mapWithMessage;
        }
        return null;
    }
}
