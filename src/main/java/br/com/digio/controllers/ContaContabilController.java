package br.com.digio.controllers;

import br.com.digio.domains.ContaContabil;
import br.com.digio.services.ContaContabilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ContaContabilController {

    @Autowired
    private ContaContabilService contaContabilService;

    @PostMapping(value = "/conta-contabil")
    public ResponseEntity<Map<String, String>> saveContaContabil(@RequestBody ContaContabil contaContabil) {
        if (contaContabil == null || contaContabil.getDescricao() == null) {
            Map<String, String> mapWithError = new HashMap<>();
            mapWithError.put("erro", "descricao é obrigatória");
            return new ResponseEntity<Map<String, String>>(mapWithError, HttpStatus.BAD_REQUEST);
        }
        contaContabil = contaContabilService.save(contaContabil);
        Map<String, String> mapWithId = new HashMap<>();
        mapWithId.put("id", String.valueOf(contaContabil.getId()));
        return new ResponseEntity<Map<String, String>>(mapWithId, HttpStatus.CREATED);
    }
}
