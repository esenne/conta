# ContaAPI #

Digio recruitment assestment

## Usage ##

### Command line ###

From project's source code, execute:

./gradlew bootRun

### IDE ###

Open class ContaApplication and run main method

## Remarks ##

Before execute /lancamentos-contabeis endpoints, a contaContabil should be created.

POST /conta-contabil
{
    "descricao": "descricao da conta"
}

The generated id should be used with the /lancamentos-contabeis endpoints.

I created this entity because in the assestment description we have this comment:

"Um lançamento contábil é formado por uma conta contábil (número e descrição)"

Then I just assumed the ContaContabil should be another entity.

## TODO ##

1) Add UT for Controller and Repository layer and Serializer/Deserializar classes
2) Handle concurrency and return status code 409 (conflict) if that happens
